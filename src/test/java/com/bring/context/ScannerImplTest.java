package com.bring.context;

import com.bring.context.scanner.config.ScanConfig1;
import com.bring.context.scanner.config.ScanConfig2;
import com.bring.context.scanner.config.ScanRootConfig;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ScannerImplTest {

    @Test
    @DisplayName("Scan root packages")
    void scanRootPackage() {
        var scanner = new ScannerImpl();

        assertAll(
            () -> assertThat(scanner.getPackages().get(0)).isEqualTo("com.bring.context"),
            () -> assertThat(scanner.getPackages().get(1)).isEqualTo("com.bring.context.scanner"),
            () -> assertThat(scanner.getPackages().get(2)).isEqualTo("com.google"),
            () -> assertThat(scanner.getPackages().get(3)).isEqualTo("com.apple"),
            () -> assertThat(scanner.getPackages()).hasSize(4),
            () -> assertThat(scanner.getClasses()).hasSize(11),
            () -> assertThat(scanner.getConfigurations()).hasSize(3),
            () -> assertThat(scanner.getBeans()).hasSize(3),
            () -> assertNotNull(scanner.getBeanPostProcessors()),
            () -> assertThat(scanner.getBeanPostProcessors()).hasSize(1)
        );
    }

    @Test
    @DisplayName("Scan the provided package by the configuration")
    void scanProvidedConfiguration() {
        var scanner = new ScannerImpl(ScanRootConfig.class);

        assertAll(
            () -> assertThat(scanner.getPackages()).hasSize(1),
            () -> assertThat(scanner.getPackages().get(0)).isEqualTo("com.bring.context.scanner"),
            () -> assertThat(scanner.getClasses()).hasSize(5),
            () -> assertThat(scanner.getConfigurations()).hasSize(1),
            () -> assertThat(scanner.getBeans()).hasSize(1),
            () -> assertNotNull(scanner.getBeanPostProcessors()),
            () -> assertThat(scanner.getBeanPostProcessors()).hasSize(1)
        );
    }

    @Test
    @DisplayName("Scan the list of provided configurations")
    void scanConfigurations() {
        Scanner scanner = new ScannerImpl(ScanRootConfig.class, ScanConfig1.class, ScanConfig2.class);

        assertAll(
            () -> assertThat(scanner.getPackages()).hasSize(3),
            () -> assertThat(scanner.getPackages().get(0)).isEqualTo("com.bring.context.scanner"),
            () -> assertThat(scanner.getPackages().get(1)).isEqualTo("com.google"),
            () -> assertThat(scanner.getPackages().get(2)).isEqualTo("com.apple"),
            () -> assertThat(scanner.getClasses()).hasSize(5),
            () -> assertThat(scanner.getConfigurations()).hasSize(3),
            () -> assertThat(scanner.getBeans()).hasSize(3),
            () -> assertNotNull(scanner.getBeanPostProcessors()),
            () -> assertThat(scanner.getBeanPostProcessors()).hasSize(1)
        );
    }

    @Test
    @DisplayName("Scan the provided package")
    void scanProvidedPackage() {
        var scanner = new ScannerImpl("com.bring.context");

        assertAll(
            () -> assertThat(scanner.getPackages().get(0)).isEqualTo("com.bring.context"),
            () -> assertThat(scanner.getPackages().get(1)).isEqualTo("com.bring.context.scanner"),
            () -> assertThat(scanner.getPackages().get(2)).isEqualTo("com.google"),
            () -> assertThat(scanner.getPackages().get(3)).isEqualTo("com.apple"),
            () -> assertThat(scanner.getPackages()).hasSize(4),
            () -> assertThat(scanner.getClasses()).hasSize(11),
            () -> assertThat(scanner.getConfigurations()).hasSize(3),
            () -> assertThat(scanner.getBeans()).hasSize(3),
            () -> assertNotNull(scanner.getBeanPostProcessors()),
            () -> assertThat(scanner.getBeanPostProcessors()).hasSize(1)
        );
    }

    @Test
    @DisplayName("Scan the provided packages")
    void scanPackages() {
        var scanner = new ScannerImpl(
            "com.bring.context.scanner.component", "com.bring.context.scanner.controller"
        );
        assertAll(
            () -> assertThat(scanner.getPackages().get(0)).isEqualTo("com.bring.context.scanner.component"),
            () -> assertThat(scanner.getPackages().get(1)).isEqualTo("com.bring.context.scanner.controller"),
            () -> assertThat(scanner.getPackages()).hasSize(2),
            () -> assertThat(scanner.getClasses()).hasSize(3),
            () -> assertThat(scanner.getConfigurations()).hasSize(0),
            () -> assertThat(scanner.getBeans()).hasSize(0),
            () -> assertNotNull(scanner.getBeanPostProcessors()),
            () -> assertThat(scanner.getBeanPostProcessors()).hasSize(1)
        );
    }
}
