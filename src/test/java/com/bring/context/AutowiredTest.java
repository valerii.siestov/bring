package com.bring.context;

import com.bring.context.autowired.FacadeService;
import com.bring.context.autowired.PrimaryService;
import com.bring.context.autowired.RepositoryComponent;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AutowiredTest {

    private AnnotationConfigWebApplicationContext context;

    @BeforeAll
    public void setup() {
        context = new AnnotationConfigWebApplicationContext("com.bring.context.autowired");
    }

    @Test
    void assertBeansAreCreated() {
        RepositoryComponent repository = context.getBean(RepositoryComponent.class);
        PrimaryService primaryService = context.getBean(PrimaryService.class);
        FacadeService facadeService = context.getBean(FacadeService.class);

        assertAll(
            () -> assertNotNull(repository),
            () -> assertNotNull(primaryService),
            () -> assertNotNull(facadeService)
        );
    }

    @Test
    void dependencyIsInjectedWhenAnnotatedWithAutowired() {
        RepositoryComponent repositoryComponent = context.getBean(RepositoryComponent.class);
        PrimaryService primaryService = context.getBean(PrimaryService.class);

        assertThat(repositoryComponent.someRepositoryMethod()).isEqualTo("someRepositoryMethod");
        assertThat(primaryService.somePrimaryServiceMethod())
            .isEqualTo("somePrimaryServiceMethod called someRepositoryMethod");
    }

    @Test
    void beanDependsOnAnotherBeanWithDependency() {
        RepositoryComponent repositoryComponent = context.getBean(RepositoryComponent.class);
        PrimaryService primaryService = context.getBean(PrimaryService.class);
        FacadeService facadeService = context.getBean(FacadeService.class);

        assertThat(repositoryComponent.someRepositoryMethod()).isEqualTo("someRepositoryMethod");
        assertThat(primaryService.somePrimaryServiceMethod())
            .isEqualTo("somePrimaryServiceMethod called someRepositoryMethod");
        assertThat(facadeService.someFacadeServiceMethod())
            .isEqualTo("someFacadeServiceMethod called somePrimaryServiceMethod called someRepositoryMethod");
    }
}
