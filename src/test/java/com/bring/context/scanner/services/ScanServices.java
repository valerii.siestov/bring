package com.bring.context.scanner.services;

import com.bring.stereotype.Service;

@Service
public class ScanServices {
    public String serviceMethod() {
        return this.getClass().getSimpleName();
    }
}
