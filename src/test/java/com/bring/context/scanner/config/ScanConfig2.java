package com.bring.context.scanner.config;

import com.bring.context.annotations.Bean;
import com.bring.context.annotations.ComponentScan;
import com.bring.context.annotations.Configuration;

@Configuration
@ComponentScan("com.apple")
public class ScanConfig2 {
    @Bean
    public ContactProvider getContactProvider(EmailProvider email, NameProvider name) {
        return new ContactProvider(email, name);
    }
}
