package com.bring.context.scanner.config;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ContactProvider {
    private final EmailProvider email;
    private final NameProvider name;

    public String getIdentity() {
        return String.format("%s - %s", name.sayHello(), email.printEmail());
    }
}

