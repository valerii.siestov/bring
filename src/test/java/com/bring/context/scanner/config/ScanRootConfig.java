package com.bring.context.scanner.config;

import com.bring.context.annotations.Bean;
import com.bring.context.annotations.ComponentScan;
import com.bring.context.annotations.Configuration;

@Configuration
@ComponentScan("com.bring.context.scanner")
public class ScanRootConfig {
    @Bean
    public NameProvider getNameProvider() {
        return new NameProvider();
    }
}

