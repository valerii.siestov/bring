package com.bring.context.scanner.config;

import com.bring.context.annotations.Bean;
import com.bring.context.annotations.ComponentScan;
import com.bring.context.annotations.Configuration;

@Configuration
@ComponentScan("com.google")
public class ScanConfig1 {
    @Bean
    public EmailProvider getEmailProvider() {
        return new EmailProvider();
    }
}
