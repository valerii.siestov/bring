package com.bring.context;

import com.bring.context.factory.*;
import com.bring.context.scanner.config.*;
import com.bring.exception.CircularDependencyException;
import com.bring.exception.MultipleConstructorsException;
import com.bring.exception.NotFoundDependencyException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BeanFactoryImplTest {
    @Test
    @DisplayName("Create instance with no argument constructor")
    void createInstanceWithNoArgumentConstructor() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryNotDepsService.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryNotDepsService service = beanFactory.getBean(FactoryNotDepsService.class);

        assertEquals("Hello", service.getMessage());
    }

    @Test
    @DisplayName("Create instance with one argument constructor")
    void createInstanceWithOneArgumentConstructor() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryOneDepService.class);
        classes.add(FactoryNotDepsService.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryOneDepService service = beanFactory.getBean(FactoryOneDepService.class);

        assertEquals("Hello", service.getMessage());
    }

    @Test
    @DisplayName("Create instance with multiple arguments constructor")
    void createInstanceWithMultipleArgumentConstructor() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryDep1Service.class);
        classes.add(FactoryDep2Service.class);
        classes.add(FactoryDep3Service.class);
        classes.add(FactoryDep4Service.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryDep4Service service = beanFactory.getBean(FactoryDep4Service.class);

        assertEquals("com.bring.context.factory.FactoryDep4Service", service.getDependencyMethod());
        assertEquals("com.bring.context.factory.FactoryDep1Service", service.getDep1Method());
        assertEquals("com.bring.context.factory.FactoryDep2Service", service.getDep2Method());
        assertEquals("com.bring.context.factory.FactoryDep3Service", service.getDep3Method());
    }

    @Test
    @DisplayName("Catch not found dependency")
    void catchNotFoundDependency() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryDep1Service.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        assertThrows(NotFoundDependencyException.class, () -> beanFactory.getBean(FactoryDep4Service.class));
    }

    @Test
    @DisplayName("Catch multiple constructors exception")
    void catchMultipleConstructors() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryDep1Service.class);
        classes.add(FactoryDep2Service.class);
        classes.add(FactoryDepMultipleConstructor.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        assertThrows(MultipleConstructorsException.class, () -> beanFactory.getBean(FactoryDepMultipleConstructor.class));
    }

    @Test
    @DisplayName("Create instance with private constructor")
    void createInstanceWithPrivateConstructor() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryPrivateConstructorNoArgs.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryPrivateConstructorNoArgs factoryPrivateConstructorNoArgs = beanFactory
            .getBean(FactoryPrivateConstructorNoArgs.class);

        assertEquals("Hello", factoryPrivateConstructorNoArgs.getMessage());
    }

    @Test
    @DisplayName("Create instance with private constructor and dependencies")
    void createInstanceWithPrivateConstructorAndDependencies() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryDep1Service.class);
        classes.add(FactoryDep2Service.class);
        classes.add(FactoryPrivateConstructorWithArgs.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryPrivateConstructorWithArgs FactoryPrivateConstructorWithArgs = beanFactory
            .getBean(FactoryPrivateConstructorWithArgs.class);

        assertEquals("com.bring.context.factory.FactoryDep1Service", FactoryPrivateConstructorWithArgs.getMessage());
    }

    @Test
    @DisplayName("Circular dependency detection")
    void circularDependencyDetection() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryCircular1Service.class);
        classes.add(FactoryCircular2Service.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        assertThrows(CircularDependencyException.class, () -> beanFactory.getBean(FactoryCircular1Service.class));
    }

    @Test
    @DisplayName("Get config bean instances")
    void getConfigBeanInstance() {
        Scanner scanner = new ScannerImpl(ScanRootConfig.class, ScanConfig1.class, ScanConfig2.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(new HashSet<>(), scanner.getBeans());

        NameProvider nameProvider = beanFactory.getBean(NameProvider.class);
        EmailProvider emailProvider = beanFactory.getBean(EmailProvider.class);
        ContactProvider contactProvider = beanFactory.getBean(ContactProvider.class);

        assertEquals("Hello", nameProvider.sayHello());
        assertEquals("valerii.siestov@gmail.com", emailProvider.printEmail());
        assertEquals("Hello - valerii.siestov@gmail.com", contactProvider.getIdentity());
    }

    @Test
    @DisplayName("Get config bean instances exceptions")
    void getConfigBeanInstanceExceptions() {
        Scanner scanner = new ScannerImpl(ScanConfig1.class, ScanConfig2.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(new HashSet<>(), scanner.getBeans());
        EmailProvider emailProvider = beanFactory.getBean(EmailProvider.class);

        assertThrows(NotFoundDependencyException.class, () -> beanFactory.getBean(NameProvider.class));
        assertThrows(NotFoundDependencyException.class, () -> beanFactory.getBean(ContactProvider.class));

        assertEquals("valerii.siestov@gmail.com", emailProvider.printEmail());
    }

    @Test
    @DisplayName("Check dependency injection mixed with constructor and config brean")
    void checkDependencyWithConstructorAndBean() {
        Scanner scanner = new ScannerImpl(ScanConfig1.class);
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryWithConfigBeanDependency.class);
        classes.add(FactoryDep1Service.class);

        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, scanner.getBeans());

        FactoryWithConfigBeanDependency factoryWithConfigBeanDependency = beanFactory
            .getBean(FactoryWithConfigBeanDependency.class);

        assertEquals("valerii.siestov@gmail.com", factoryWithConfigBeanDependency.getEmail());
        assertEquals("com.bring.context.factory.FactoryDep1Service", factoryWithConfigBeanDependency.getDependencyName());
    }

    @Test
    @DisplayName("Create interface implementation instance")
    void createInterfaceImplementationInstance() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryInterfaceImpl.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryInterface factoryInterface = beanFactory.getBean(FactoryInterface.class);

        assertEquals("eat", factoryInterface.eat());
        assertEquals("code", factoryInterface.code());
        assertEquals("sleep", factoryInterface.sleep());
        assertEquals("repeat", factoryInterface.repeat());
    }

    @Test
    @DisplayName("Create bean instance with interface and qualifiers")
    void createBeanInstanceWithInterfacesAndQualifiers() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(FactoryInterfaceImpl.class);
        classes.add(FactoryInterfaceWithQualifier.class);
        classes.add(FactoryWithInterfaces.class);
        BeanFactoryImpl beanFactory = new BeanFactoryImpl(classes, new HashMap<>());

        FactoryWithInterfaces factoryWithInterfaces = beanFactory.getBean(FactoryWithInterfaces.class);

        assertEquals("eat code sleep repeat", factoryWithInterfaces.getFactory1Output());
        assertEquals("eat less code more what? again?", factoryWithInterfaces.getFactory2Output());
    }
}
