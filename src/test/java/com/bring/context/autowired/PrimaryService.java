package com.bring.context.autowired;

import com.bring.factory.annotation.Autowired;
import com.bring.stereotype.Service;

@Service
public class PrimaryService {
    @Autowired
    private RepositoryComponent repositoryComponent;

    public String somePrimaryServiceMethod() {
        return "somePrimaryServiceMethod called " + repositoryComponent.someRepositoryMethod();
    }
}
