package com.bring.context.autowired;

import com.bring.factory.annotation.Autowired;
import com.bring.stereotype.Service;

@Service
public class FacadeService {
    @Autowired
    private PrimaryService primaryService;

    public String someFacadeServiceMethod() {
        return "someFacadeServiceMethod called " + primaryService.somePrimaryServiceMethod();
    }
}
