package com.bring.context.autowired;

import com.bring.stereotype.Repository;

@Repository
public class RepositoryComponent {

    public String someRepositoryMethod() {
        return "someRepositoryMethod";
    }
}
