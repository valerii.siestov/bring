package com.bring.context.factory;

public class FactoryCircular1Service {
    private final FactoryCircular2Service factoryCircular2Service;

    public FactoryCircular1Service(FactoryCircular2Service factoryCircular2Service) {
        this.factoryCircular2Service = factoryCircular2Service;
    }
}
