package com.bring.context.factory;

public class FactoryDep4Service {
    private final FactoryDep1Service factoryDep1Service;
    private final FactoryDep2Service factoryDep2Service;
    private final FactoryDep3Service factoryDep3Service;

    public FactoryDep4Service(
        FactoryDep1Service factoryDep1Service,
        FactoryDep2Service factoryDep2Service,
        FactoryDep3Service factoryDep3Service
    ) {
        this.factoryDep1Service = factoryDep1Service;
        this.factoryDep2Service = factoryDep2Service;
        this.factoryDep3Service = factoryDep3Service;
    }

    public String getDependencyMethod() {
        return this.getClass().getTypeName();
    }

    public String getDep1Method() {
        return factoryDep1Service.getDependencyIdentity();
    }

    public String getDep2Method() {
        return factoryDep2Service.getDependencyMethod();
    }

    public String getDep3Method() {
        return factoryDep3Service.getDependencyMethod();
    }
}
