package com.bring.context.factory;

public class FactoryPrivateConstructorWithArgs {
    private final FactoryDep1Service factoryDep1Service;
    private final FactoryDep2Service factoryDep2Service;

    private FactoryPrivateConstructorWithArgs(
        FactoryDep1Service factoryDep1Service,
        FactoryDep2Service factoryDep2Service
    ) {
        this.factoryDep1Service = factoryDep1Service;
        this.factoryDep2Service = factoryDep2Service;
    }

    public String getMessage() {
        return this.factoryDep1Service.getDependencyIdentity();
    }
}
