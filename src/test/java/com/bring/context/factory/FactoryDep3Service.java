package com.bring.context.factory;

public class FactoryDep3Service {
    private final FactoryDep2Service factoryDep2Service;

    public FactoryDep3Service(FactoryDep2Service factoryDep2Service) {
        this.factoryDep2Service = factoryDep2Service;
    }

    public String getDependencyMethod() {
        return this.getClass().getTypeName();
    }
}
