package com.bring.context.factory;

import com.bring.context.annotations.Component;

@Component
public class FactoryInterfaceImpl implements FactoryInterface {
    @Override
    public String eat() {
        return "eat";
    }

    @Override
    public String code() {
        return "code";
    }

    @Override
    public String sleep() {
        return "sleep";
    }

    @Override
    public String repeat() {
        return "repeat";
    }
}
