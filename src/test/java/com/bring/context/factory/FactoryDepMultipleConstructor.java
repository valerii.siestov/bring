package com.bring.context.factory;

public class FactoryDepMultipleConstructor {
    private FactoryDep1Service factoryDep1Service;
    private FactoryDep2Service factoryDep2Service;

    public FactoryDepMultipleConstructor(FactoryDep1Service factoryDep1Service) {
        this.factoryDep1Service = factoryDep1Service;
    }

    public FactoryDepMultipleConstructor(FactoryDep1Service factoryDep1Service, FactoryDep2Service factoryDep2Service) {
        this.factoryDep1Service = factoryDep1Service;
        this.factoryDep2Service = factoryDep2Service;
    }
}
