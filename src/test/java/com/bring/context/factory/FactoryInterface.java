package com.bring.context.factory;

public interface FactoryInterface {
    String eat();
    String code();
    String sleep();
    String repeat();
}
