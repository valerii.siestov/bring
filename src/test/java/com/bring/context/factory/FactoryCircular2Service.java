package com.bring.context.factory;

public class FactoryCircular2Service {
    private final FactoryCircular1Service factoryCircular1Service;

    public FactoryCircular2Service(FactoryCircular1Service factoryCircular1Service) {
        this.factoryCircular1Service = factoryCircular1Service;
    }
}
