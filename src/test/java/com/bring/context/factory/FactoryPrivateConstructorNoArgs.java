package com.bring.context.factory;

public class FactoryPrivateConstructorNoArgs {
    private FactoryPrivateConstructorNoArgs() {
    }

    public String getMessage() {
        return "Hello";
    }
}
