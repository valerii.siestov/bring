package com.bring.context.factory;

import com.bring.factory.annotation.Qualifier;
import lombok.RequiredArgsConstructor;

public class FactoryWithInterfaces {
    private final FactoryInterface factoryInterface1;

    @Qualifier("FactoryInterfaceWithQualifier")
    private final FactoryInterface factoryInterface2;

    public FactoryWithInterfaces(
        @Qualifier("FactoryInterfaceImpl") FactoryInterface factoryInterface1,
        FactoryInterface factoryInterface2
    ) {
        this.factoryInterface1 = factoryInterface1;
        this.factoryInterface2 = factoryInterface2;
    }

    public String getFactory1Output() {
        return String.format(
            "%s %s %s %s",
            factoryInterface1.eat(),
            factoryInterface1.code(),
            factoryInterface1.sleep(),
            factoryInterface1.repeat()
        );
    }

    public String getFactory2Output() {
        return String.format(
            "%s %s %s %s",
            factoryInterface2.eat(),
            factoryInterface2.code(),
            factoryInterface2.sleep(),
            factoryInterface2.repeat()
        );
    }
}
