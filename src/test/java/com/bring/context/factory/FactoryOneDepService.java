package com.bring.context.factory;

public class FactoryOneDepService {
    private final FactoryNotDepsService factoryNotDepsService;

    public FactoryOneDepService(FactoryNotDepsService factoryNotDepsService) {
        this.factoryNotDepsService = factoryNotDepsService;
    }

    public String getMessage() {
        return factoryNotDepsService.getMessage();
    }
}
