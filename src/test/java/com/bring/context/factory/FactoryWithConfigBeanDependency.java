package com.bring.context.factory;

import com.bring.context.scanner.config.EmailProvider;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FactoryWithConfigBeanDependency {
    private final EmailProvider emailProvider;
    private final FactoryDep1Service factoryDep1Service;

    public String getEmail() {
        return this.emailProvider.printEmail();
    }

    public String getDependencyName() {
        return this.factoryDep1Service.getDependencyIdentity();
    }
}
