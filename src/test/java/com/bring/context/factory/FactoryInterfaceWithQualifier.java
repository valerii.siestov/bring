package com.bring.context.factory;

import com.bring.context.annotations.Component;

@Component
public class FactoryInterfaceWithQualifier implements FactoryInterface {
    @Override
    public String eat() {
        return "eat less";
    }

    @Override
    public String code() {
        return "code more";
    }

    @Override
    public String sleep() {
        return "what?";
    }

    @Override
    public String repeat() {
        return "again?";
    }
}
