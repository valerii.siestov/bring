package com.bring.context;

import com.bring.context.scanner.config.NameProvider;
import com.bring.context.scanner.config.ScanRootConfig;
import com.bring.context.scanner.services.ScanServices;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnnotationConfigWebApplicationContextTest {
    @Test
    @DisplayName("Create application context using configs")
    void createApplicationContextUsingConfigs() {
        var context = new AnnotationConfigWebApplicationContext(ScanRootConfig.class);

        NameProvider nameProvider = context.getBean(NameProvider.class);

        assertEquals("Hello", nameProvider.sayHello());
    }

    @Test
    @DisplayName("Create application context using packages")
    void createApplicationContextUsingPackages() {
        var context = new AnnotationConfigWebApplicationContext("com.bring.context");

        ScanServices scanServices = context.getBean(ScanServices.class);

        assertEquals("ScanServices", scanServices.serviceMethod());
    }
}
