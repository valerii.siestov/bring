package com.bring.exception;

public class BeanPostProcessException extends RuntimeException {
    public BeanPostProcessException() {
        super();
    }

    public BeanPostProcessException(Throwable cause) {
        super(cause);
    }
}
