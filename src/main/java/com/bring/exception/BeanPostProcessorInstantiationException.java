package com.bring.exception;

/**
 * This exception is thrown in case something goes wrong while scanning for classes that implement interface
 * {@link com.bring.context.postprocessor.BeanPostProcessor} and creating their instances.
 *
 */
public class BeanPostProcessorInstantiationException extends RuntimeException {
    public BeanPostProcessorInstantiationException() {
        super();
    }

    public BeanPostProcessorInstantiationException(Throwable cause) {
        super(cause);
    }
}
