package com.bring.context;

import com.bring.exception.CircularDependencyException;
import com.bring.exception.CreateDependencyException;
import com.bring.exception.MultipleConstructorsException;
import com.bring.exception.NotFoundDependencyException;
import com.bring.factory.annotation.Qualifier;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;
import java.util.Objects;

public class BeanFactoryImpl implements BeanFactory {
    private final Set<Class<?>> classes;
    private final Map<Class<?>, ConfigBean> configBeans;
    private final Map<Class<?>, Object> di = new HashMap<>();

    public BeanFactoryImpl(Set<Class<?>> classes, Map<Class<?>, ConfigBean> configBeans) {
        this.classes = classes;
        this.configBeans = configBeans;
    }

    private <T> T createNoDependencyInstance(Class<T> classItem, Constructor<?> constructor) {
        Object instance = null;
        try {
            instance = constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw new CreateDependencyException();
        }

        di.put(classItem, instance);

        return classItem.cast(instance);
    }

    private String getQualifier(Class<?> classItem, Parameter param) {
        Qualifier qualifier = param.getDeclaredAnnotation(Qualifier.class);

        if (qualifier != null) {
            return qualifier.value();
        }

        return Arrays.stream(classItem.getDeclaredFields())
            .filter(field -> field.getDeclaredAnnotation(Qualifier.class) != null && field.getType().equals(param.getType()))
            .findFirst()
            .map(field -> field.getDeclaredAnnotation(Qualifier.class).value())
            .orElse(null);
    }

    private <T> T createWithDependencyInstance(Class<T> classItem, Constructor<?> constructor, Set<Class<?>> dependencies) {
        int parametersCount = constructor.getParameterCount();
        Parameter[] params = constructor.getParameters();
        Object[] deps = new Object[parametersCount];

        for (int i = 0; i < parametersCount; i++) {
            deps[i] = getInstance(params[i].getType(), dependencies, getQualifier(classItem, params[i]));
        }

        Object instance = null;

        try {
            instance = constructor.newInstance(deps);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw new CreateDependencyException();
        }

        di.put(classItem, instance);

        return classItem.cast(instance);
    }

    private Class<?> findInterfaceImplementation(Class<?> classItem, String qualifier) {
        if (classItem.isInterface()) {
            return classes.stream()
                .filter(item -> item.getInterfaces().length > 0)
                .map(item -> Arrays.stream(item.getInterfaces())
                    .anyMatch(iface -> iface == classItem && (qualifier == null || item.getSimpleName().equals(qualifier)))
                        ? item
                        : null
                )
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(NotFoundDependencyException::new);
        }

        return classItem;
    }

    private <T> T getInstance(Class<T> classItem, Set<Class<?>> dependencies, String qualifier) {
        Class<T> implementationClassItem = (Class<T>)findInterfaceImplementation(classItem, qualifier);

        if (di.containsKey(implementationClassItem)) {
            return classItem.cast(di.get(implementationClassItem));
        }

        if (configBeans.containsKey(implementationClassItem)) {
            return getBeanInstance(implementationClassItem, dependencies);
        }

        return getConstructorInstance(implementationClassItem, dependencies);
    }

    private <T> T getInstance(Class<T> classItem, Set<Class<?>> dependencies) {
        return getInstance(classItem, dependencies, null);
    }

    private <T> T getBeanInstance(Class<T> classItem, Set<Class<?>> dependencies) {
        if (dependencies.contains(classItem)) {
            throw new CircularDependencyException();
        }

        dependencies.add(classItem);

        ConfigBean bean = configBeans.get(classItem);

        Object[] params = Arrays.stream(bean.getMethod().getParameterTypes())
            .map(type -> getInstance(type, dependencies)).toArray();

        T instance = null;
        try {
            instance = classItem.cast(bean.getMethod().invoke(bean.getInstance(), params));
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw new CreateDependencyException();
        }

        di.put(classItem, instance);

        return instance;
    }

    private <T> T getConstructorInstance(Class<T> classItem, Set<Class<?>> dependencies) {
        if (dependencies.contains(classItem)) {
            throw new CircularDependencyException();
        }

        dependencies.add(classItem);

        if (!classes.contains(classItem)) {
            throw new NotFoundDependencyException();
        }

        if (classItem.getDeclaredConstructors().length > 1) {
            throw new MultipleConstructorsException();
        }

        Constructor<?> constructor = classItem.getDeclaredConstructors()[0];
        constructor.setAccessible(true);

        if (constructor.getParameterCount() == 0) {
            return this.createNoDependencyInstance(classItem, constructor);
        }

        return this.createWithDependencyInstance(classItem, constructor, dependencies);
    }

    /**
     * Get bean instance
     *
     * @param <T> classItem
     * @return object instance
     */
    @Override
    public <T> T getBean(Class<T> classItem) {
        Set<Class<?>> dependencies = new HashSet<>();

        return getInstance(classItem, dependencies);
    }
}
