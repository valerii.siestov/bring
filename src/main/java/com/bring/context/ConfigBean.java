package com.bring.context;

import lombok.Builder;
import lombok.Getter;

import java.lang.reflect.Method;

@Builder
@Getter
public class ConfigBean {
    private Class<?> type;
    private Object instance;
    private Method method;
}
