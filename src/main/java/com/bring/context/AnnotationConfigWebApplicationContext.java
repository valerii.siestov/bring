package com.bring.context;

import com.bring.exception.NotFoundDependencyException;

import java.util.HashMap;
import java.util.Map;

public class AnnotationConfigWebApplicationContext implements ApplicationContext {
    private final Scanner scanner;
    private final Map<Class<?>, Object> beans = new HashMap<>();
    private final BeanFactory beanFactory;

    public AnnotationConfigWebApplicationContext(Class<?>... configs) {
        scanner = new ScannerImpl(configs);
        beanFactory = new BeanFactoryImpl(scanner.getClasses(), scanner.getBeans());
        instantiateBeans();
        postProcessBeans();
    }

    public AnnotationConfigWebApplicationContext(String... packages) {
        scanner = new ScannerImpl(packages);
        beanFactory = new BeanFactoryImpl(scanner.getClasses(), scanner.getBeans());
        instantiateBeans();
        postProcessBeans();
    }

    private void instantiateBeans() {
        scanner.getClasses().forEach(classItem -> beans.put(classItem, beanFactory.getBean(classItem)));
        scanner.getBeans().forEach((aClass, configBean) -> beans.put(aClass, beanFactory.getBean(aClass)));
    }

    private void postProcessBeans() {
        beans.values().forEach(bean ->
                scanner.getBeanPostProcessors().forEach(bpp -> bpp.postProcess(bean, this)));
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        if (!beans.containsKey(beanClass)) {
            throw new NotFoundDependencyException();
        }

        return beanClass.cast(beans.get(beanClass));
    }
}
