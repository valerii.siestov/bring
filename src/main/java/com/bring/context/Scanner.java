package com.bring.context;

import com.bring.context.postprocessor.BeanPostProcessor;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface Scanner {

    List<String> getPackages();

    Set<Class<?>> getClasses();

    Set<Class<?>> getConfigurations();

    List<BeanPostProcessor> getBeanPostProcessors();

    Map<Class<?>, ConfigBean> getBeans();
}
