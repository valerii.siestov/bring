package com.bring.context;

import com.bring.context.annotations.Bean;
import com.bring.context.annotations.Component;
import com.bring.context.annotations.ComponentScan;
import com.bring.context.annotations.Configuration;
import com.bring.context.postprocessor.BeanPostProcessor;
import com.bring.exception.BeanPostProcessorInstantiationException;
import com.bring.exception.CreateDependencyException;
import com.bring.stereotype.Controller;
import com.bring.stereotype.Repository;
import com.bring.stereotype.Service;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class ScannerImpl implements Scanner {
    private static final String INFRASTRUCTURE_BEAN_POST_PROCESSORS_ROOT = "com.bring.context.postprocessor";

    private final List<String> packages = new ArrayList<>();
    private final Set<Class<?>> configurations = new HashSet<>();
    private final Set<Class<?>> classes = new HashSet<>();
    private final List<BeanPostProcessor> beanPostProcessors = new ArrayList<>();

    public ScannerImpl() {
        var packageName = getClass().getPackage().getName();
        packages.add(packageName);
        readingPackagesToFindConfiguration(packageName);
        scanForBeanPostProcessors();
        init(packages);
    }

    public ScannerImpl(String... packages) {
        this.packages.addAll(Arrays.asList(packages));
        readingPackagesToFindConfiguration(packages);
        scanForBeanPostProcessors();
        init(this.packages);
    }

    public ScannerImpl(Class<?>... configs) {
        configurations.addAll(Arrays.asList(configs));
        readingClassesToFindConfiguration(configs);
        scanForBeanPostProcessors();
        init(packages);
    }

    private void readingPackagesToFindConfiguration(String... packages) {
        Stream.of(packages)
            .map(Reflections::new)
            .flatMap(reflections -> Stream.of(reflections.getTypesAnnotatedWith(Configuration.class)))
            .peek(configurations::addAll)
            .forEach(this::addPackages);
    }

    private void readingClassesToFindConfiguration(Class<?>... configs) {
        Stream.of(configs)
            .map(this::getPackagePath)
            .forEach(packages::add);
    }

    private void addPackages(Set<Class<?>> classes) {
        classes.stream()
            .map(this::getPackagePath)
            .forEach(this.packages::add);
    }

    private String getPackagePath(Class<?> config) {
        var scanAnnotation = config.getAnnotation(ComponentScan.class);
        return scanAnnotation == null
            ? config.getPackage().getName()
            : scanAnnotation.value();
    }

    private void init(List<String> packages) {
        packages.stream()
            .map(Reflections::new)
            .flatMap(reflections -> Stream.of(
                reflections.getTypesAnnotatedWith(Component.class),
                reflections.getTypesAnnotatedWith(Service.class),
                reflections.getTypesAnnotatedWith(Controller.class),
                reflections.getTypesAnnotatedWith(Repository.class)))
            .forEach(classes::addAll);
    }

    private Object getConfigurationInstance(Class<?> configuration) {
        try {
            return configuration.getConstructors()[0].newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            throw new CreateDependencyException();
        }
    }

    private void scanForBeanPostProcessors() {
        Reflections infrastructureBeanPostProcessorScan = new Reflections(INFRASTRUCTURE_BEAN_POST_PROCESSORS_ROOT);
        infrastructureBeanPostProcessorScan.getSubTypesOf(BeanPostProcessor.class).forEach((bppClass -> {
            try {
                beanPostProcessors.add(bppClass.getDeclaredConstructor().newInstance());
            } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException e) {
                throw new BeanPostProcessorInstantiationException(e);
            }
        }));
    }

    /**
     * Get list of the scanned packages
     *
     * @return the list of packages
     */
    @Override
    public List<String> getPackages() {
        return packages;
    }

    /**
     * Get the list of the scanned classes
     *
     * @return the list of classes
     */
    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    /**
     * Get the list of configurations found within the scanned packages
     *
     * @return the list of configurations
     */
    @Override
    public Set<Class<?>> getConfigurations() {
        return configurations;
    }

    /**
     * Get the list of scanned bean post processors
     *
     * @return bean post processors
     */
    @Override
    public List<BeanPostProcessor> getBeanPostProcessors() {
        return beanPostProcessors;
    }

    /**
     * Get the map of found beans
     *
     * @return map key/value list of the type and bean definition
     */
    @Override
    public Map<Class<?>, ConfigBean> getBeans() {
        return configurations.stream().flatMap(configuration -> {
                Object instance = getConfigurationInstance(configuration);
                return Stream.of(configuration.getMethods())
                    .filter(method -> method.getAnnotation(Bean.class) != null)
                    .map(method -> ConfigBean.builder()
                        .instance(instance)
                        .type(method.getReturnType())
                        .method(method)
                        .build());
            })
            .collect(toMap(ConfigBean::getType, Function.identity()));
    }
}
