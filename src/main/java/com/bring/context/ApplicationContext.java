package com.bring.context;

public interface ApplicationContext {
    <T> T getBean(Class<T> beanClass);
}
