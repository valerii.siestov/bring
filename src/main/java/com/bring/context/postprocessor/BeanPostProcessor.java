package com.bring.context.postprocessor;

import com.bring.context.ApplicationContext;

/**
 * Classes that implement BeanPostProcessor are scanned during {@link com.bring.context.ApplicationContext} creation.
 * Classes that implement this interface are scanned and their instances are collected to a collection
 * that is encapsulated in ApplicationContext.
 *
 * Every bean stored in the context is processed by every BeanPostProcessor, kind of Chain of responsibility pattern.
 */
public interface BeanPostProcessor {
    /**
     *
      * @param bean - bean stored in the context.
     * @param context - {@link com.bring.context.ApplicationContext}
     * @return returns configured bean
     */
    default <T> T  postProcess(T bean, ApplicationContext context) {
        return bean;
    }
}
