package com.bring.context.postprocessor;

import com.bring.context.ApplicationContext;
import com.bring.exception.BeanPostProcessException;
import com.bring.factory.annotation.Autowired;

import java.lang.reflect.Field;

public class AutowiredAnnotationBeanPostProcessor implements BeanPostProcessor {

    @Override
    public <T> T postProcess(T bean, ApplicationContext context) {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Autowired.class)) {
                Object dependencyBean = context.getBean(field.getType());
                field.setAccessible(true);

                try {
                    field.set(bean, dependencyBean);
                } catch (IllegalAccessException e) {
                    throw new BeanPostProcessException(e);
                }
            }
        }
        return bean;
    }
}
