package com.bring.context;

public interface BeanFactory {
    <T> T getBean(Class<T> classItem);
}
